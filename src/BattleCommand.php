<?php namespace Console;

use Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Helper\Table;

class BattleCommand extends Command
{

    private $nextAttacker;

    public function configure()
    {
        $this->setName('battle')
            ->setDescription('Start the game.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $this->getPlayerDetails($input, $output);
        $this->renderPlayerDetails($this->player1, $output);
        $this->getPlayerDetails($input, $output);
        $this->renderPlayerDetails($this->player2, $output);
        $this->simulateBattle($output);
        return 1;
    }

    private function getPlayerDetails($input, $output)
    {
        $question = new Question('Enter player name' . "\n");
        $helper = $this->getHelper('question');
        $playerName = $helper->ask($input, $output, $question);
        $this->setCombatant($playerName, $output);
    }

    private function renderPlayerDetails($player, $output)
    {
        $table = new Table($output);
        $table
            ->setHeaders(['Name', 'Type', 'Health', 'Strength', 'Defense', 'Speed', 'Luck'])
            ->setRows([
                [
                    $player->name,
                    $player->type,
                    $player->health,
                    $player->strength,
                    $player->defense,
                    $player->speed,
                    $player->luck
                ]
            ]);
        $table->render();
    }

    private function simulateBattle($output)
    {
        $this->startBattle($output);
        //29 rounds to go
        for ($i = 2; $i < 30; $i++) {
            $output->writeln($i);
            $output->writeln($this->nextAttacker . ' Attacks !');
            if ($this->nextAttacker == 'player1') {
                $outcome = $this->player1->attack($this->player2);
                $this->nextAttacker = 'player2';
            } else {
                $outcome = $this->player2->attack($this->player1);
                $this->nextAttacker = 'player1';
            }
            $output->writeln($outcome);
            if ($this->player1->health < 1 || $this->player2->health < 1) {
                break;
            }
        }
        $this->finishBattle($output);
    }

    private function finishBattle($output) {
        if ($this->player1->health >= 1 && $this->player2->health >= 1) {
            $output->write('There has been a draw');
        } else {
            $winner = ($this->player1->health > $this->player2->health ? $this->player1 : $this->player2);
            $output->write($winner->name . ' is a true CHAMPION; For he has won');
        }
        $this->renderPlayerDetails($this->player1, $output);
        $this->renderPlayerDetails($this->player2, $output);
    }

    private function startBattle($output) {
        $outcome = '';
        if (($this->player1->speed === $this->player2->speed) && ($this->player1->defense === $this->player2->defense)) {
            $outcome = $this->player1->attack($this->player2);
            $this->nextAttacker = 'player2';
        } else {
            switch ($this->player1->speed) {
                case $this->player1->speed > $this->player2->speed:
                    $outcome = $this->player1->attack($this->player2);
                    $this->nextAttacker = 'player2';
                    break;
                case $this->player1->speed < $this->player2->speed:
                    $outcome = $this->player2->attack($this->player1);
                    $this->nextAttacker = 'player1';
                    break;
                case $this->player1->speed === $this->player2->speed:
                    if ($this->player1->defense < $this->player2->defense) {
                        $outcome = $this->player1->attack($this->player2);
                        $this->nextAttacker = 'player2';
                    } else {
                        $outcome = $this->player2->attack($this->player1);
                        $this->nextAttacker = 'player1';
                    }
                    break;
            }
        }
        //the first to hit is the player that isn't the next attacker
        $firstToHit = ($this->nextAttacker === 'player1' ? 'player2' : 'player1');
            $output->writeln(1);
        $output->writeln($firstToHit . ' Attacks !');
        $output->writeln($outcome);
    }
}