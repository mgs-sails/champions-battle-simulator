<?php


namespace Console\Models;


class Combatant
{
    public $type;
    public $name;
    public $health;
    public $strength;
    public $defense;
    public $speed;
    public $luck;
    public $missesNextAttack;

    private $attributeValues = [
        'swordsman' => [
            'min_health' => 40,
            'max_health' => 60,
            'min_strength' => 60,
            'max_strength' => 70,
            'min_defense' => 20,
            'max_defense' => 30,
            'min_speed' => 90,
            'max_speed' => 100,
            'min_luck' => 30, //%
            'max_luck' => 50 //%
        ],
        'brute' => [
            'min_health' => 90,
            'max_health' => 100,
            'min_strength' => 65,
            'max_strength' => 75,
            'min_defense' => 40,
            'max_defense' => 50,
            'min_speed' => 40,
            'max_speed' => 65,
            'min_luck' => 30,
            'max_luck' => 35
        ],
        'grappler' => [
            'min_health' => 60,
            'max_health' => 100,
            'min_strength' => 75,
            'max_strength' => 80,
            'min_defense' => 35,
            'max_defense' => 40,
            'min_speed' => 60,
            'max_speed' => 80,
            'min_luck' => 30,
            'max_luck' => 40
        ]
    ];

    public function __construct($type, $name)
    {
        $this->type = $type;
        $this->name = $name;
        $this->missesNextAttack = false;
        $this->setAttributes();
    }

    private function setAttributes()
    {
        $valuesToChooseFrom = $this->attributeValues[$this->type];
        $this->health = rand($valuesToChooseFrom['min_health'], $valuesToChooseFrom['max_health']);
        $this->strength = rand($valuesToChooseFrom['min_strength'], $valuesToChooseFrom['max_strength']);
        $this->defense = rand($valuesToChooseFrom['min_defense'], $valuesToChooseFrom['max_defense']);
        $this->speed = rand($valuesToChooseFrom['min_speed'], $valuesToChooseFrom['max_speed']);
        $this->luck = rand($valuesToChooseFrom['min_luck'], $valuesToChooseFrom['max_luck']);
    }

    public function attack(Combatant $defender)
    {
        $outcome = '';
        $damage = 0;
        if ($this->missesNextAttack) {
            $outcome = $this->name . ' was stunned by last attack; misses this turn';
            $this->missesNextAttack = false; //reset flag
            return $outcome;
        }
        if ($defender->isLucky($defender->luck)) { //Attack will miss
            $outcome = 'Unlucky ! ' . $this->name . '  missed';
            if ($defender->type === 'grappler') { //Missed attacks against grapplers results in counter
                $this->health -= 10;
                $outcome = 'COUNTER ATTACK !!!! Unlucky ! ' . $this->name . '  missed and ' . $defender->name .
                    ' countered; -10 damage dealt to Attacker. Attacker health down to ' . $this->health;
            }
            return $outcome;
        } else { //Attack will hit
            $damage = $this->strength - $defender->defense;
            if ($this->type === 'swordsman' && $this->isLucky(5)) {
                $damage *= 2;
                $defender->health -= $damage;
                $outcome = 'DOUBLE DAMAGE !!!!' . $defender->name . ' has been hit; -' . $damage .
                    ' damage dealt; health down to ' . $defender->health;
                return $outcome;
            }
            if ($this->type === 'brute' && $this->isLucky(2)) {
                $defender->missesNextAttack = true;
                $defender->health -= $damage;
                $outcome = 'STUNNER !!!!' . $defender->name . ' has been hit and stunned; -' . $damage .
                    ' damage dealt; health down to ' . $defender->health;
                return $outcome;
            }
            $defender->health -= $damage;
            $outcome = 'ITS A HIT !!! ' . $defender->name . ' has been hit; -' . $damage .
                ' damage dealt; health down to ' . $defender->health;

        }
        return $outcome;
    }

    private function isLucky($chance)
    {
        //it's been assumed that any number between 1 and 100 has a 1% chance of being randomly printed by the rand method
        //thus we determine luck by checking whether a random number between one and hundred falls within the chance
        $num = rand(1, 100);
        return $num <= $chance;
    }

}