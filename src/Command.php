<?php namespace Console;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Console\Models\Combatant;

class Command extends SymfonyCommand
{

    public $player1;
    public $player2;
    private $combatantTypes = [
        'swordsman',
        'brute',
        'grappler'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    protected function setCombatant($combatantName, OutputInterface $output)
    {
        $combatantType = $this->combatantTypes[rand(0, 2)];
        if (!$this->player1 instanceof Combatant) { //implies player 1 has not been gotten
            $this->player1 = new Combatant($combatantType, $combatantName);
        } else {
            $this->player2 = new Combatant($combatantType, $combatantName);
        }
        $standardWelcomeMessage = 'Welcome ' . $combatantName . ' You are a ' . $combatantType . "\n";
        $outputMessage = ($this->player2 instanceof Combatant ? $standardWelcomeMessage
            . 'Who shall be the CHAMPION !!!?' : $standardWelcomeMessage);
        $output->write($outputMessage);
    }
}